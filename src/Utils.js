import * as moment from 'moment';

var dataLength = 6000;
var ITEMS = ['RE', 'BMW', 'HARLEY', 'YAMAHA']
var ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVXYZ'.split('')

const getRandomValue = (items) => {
  return items[Math.round(items.length * Math.random())]
}

export const fetchData = () => {
  var data = []

  for (var i = dataLength; i > 0; i--) {
    data.push({
      name: `${getRandomValue(ALPHA)}${getRandomValue(ALPHA)}${getRandomValue(ALPHA)}${getRandomValue(ALPHA)}`,
      className: getRandomValue(ITEMS),
      time: moment()
        .subtract(i, 'days')
        .startOf('day')
        .format(),
      value: Math.round(Math.random() * 100)
    })
  }

  return data
}

const DEFAULT_OPTION = {
  xAxis: {
    type: 'time',
    splitLine: { show: false }
  },
  yAxis: {
    type: 'value',
    max: 100,
    axisLine: { show: false },
    axisLabel: { show: false },
    axisTick: { show: false },
    splitLine: { show: false }
  },
  legend: {
    type: 'scroll',
    show: true
  },
  tootip: {},
  backgroundColor: '#f1f1f1'
}

const DEFAULT_SERIES = {
  type: 'custom',
  label: {
    normal: {
      show: true,
      position: 'insideTop',
      color: 'white',
      fontSize: 10,
      textShadowBlur: 1,
      textShadowColor: '#f1f1f1',
      width: 160
    }
  },
  encode: {
    x: 'time',
    y: 'value'
  }
}

const createSeries = (data) => {
  return ITEMS.map((name) => {
    return {
      ...DEFAULT_SERIES,
      name,
      label: {
        show: false
      },
      itemStyle: {
        normal: {
          color: "#333333"
        }
      },
      renderItem (params, api) {
        var itemData = data[params.dataIndex];

        if (itemData.className !== name) {
          return false;
        }

        var startPoint = api.coord([itemData.time, itemData.value]);

        return {
          type: 'group',
          children: [{
            type: 'circle',
            shape: {
              cx: startPoint[0],
              cy: startPoint[1],
              r: 10
            }
          }]
        }
      }
    }
  })
}

export const create = (data) => {
  return {
    ...DEFAULT_OPTION,
    dataset: { source: data },
    series: createSeries(data)
  };
}
