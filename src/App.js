import React, { Component } from 'react';
import Charts from './Charts';
import { fetchData } from './Utils';

const delay = 10000

class App extends Component {
  constructor() {
    super()

    this.state = {
      version: 0,
      data: []
    }

    this.updateCount = this._updateCount.bind(this);
    this.fetchAndSet = this._fetchAndSet.bind(this);
  }

  componentDidMount () {
    this.fetchAndSet()
  }

  _fetchAndSet () {
    setTimeout(() => {
      this.setState({
        data: fetchData(),
        version: this.state.version + 1
      });
    }, delay)
  }

  _updateCount () {
    this.setState({
      version: this.state.version + 1
    })
  }

  render () {
    const { version, data } = this.state

    return (
      <div>
        <button onClick={this.updateCount}>change state</button>
        <br/>
        <Charts version={version} data={data} />
      </div>
    )
  }
}

export default App
