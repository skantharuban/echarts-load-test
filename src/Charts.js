import React, { PureComponent } from 'react';
import ReactEcharts from 'echarts-for-react';
import { create } from './Utils';

class Charts extends PureComponent {
  shouldComponentUpdate (props, state) {
    const { version: prevVersion } = this.props
    const { version: nextVersion } = props

    if (prevVersion !== nextVersion) {
      return true;
    }

    return false;
  }

  getOption () {
    const { data } = this.props

    return create(data)
  }

  render () {
    const { version } = this.props

    return (
      <ReactEcharts option={this.getOption()} />
    )
  }
}

export default Charts
